### 코로나19 예방접종 위탁의료기관 조회서비스 APP
###### 공공데이터 오픈 API 사용
![Covid](./images/covid3.png)

***

#### LANGUAGE
```
Dart 
Flutter
```
#### 기능
```

* 코로나 예방접종을 할수있는 위탁의료기관을 조회할수있다
  - 전지역 전체 조회가능
  - 원하는 지역을 선택해서 조회 가능
  - 점심시간 및 진료시간 확인 가능
  - 기관 위치 확인
  - 휴무일 여부 확인

```


# app 화면

### 전국에 위탁의료기관 조회
![Covid](./images/covid1.png)

### 원하는 지역을 선택
![Covid](./images/covid2.png)


### 원하는 지역을 선택해 그지역의 위탁의료기관 조회
![Covid](./images/covid.png)
