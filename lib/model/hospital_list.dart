import 'package:app_api_test1/model/hospital_list_item.dart';

class HospitalList {
  int page;
  int perPage;
  int totalCount;
  int currentCount;
  int matchCount;
  List<HospitalListItem>? data;  //null

  HospitalList(
      this.page,
      this.perPage,
      this.totalCount,
      this.currentCount,
      this.matchCount,
      {this.data});

  factory HospitalList.fromJson(Map<String, dynamic> json) {
    return HospitalList(
      json['page'],
      json['perPage'],
      json['totalCount'],
      json['currentCount'],
      json['matchCount'],
      data: json['data'] != null ? (json['data'] as List).map((e) => HospitalListItem.fromJson(e)).toList() : null,
    );
  }
}