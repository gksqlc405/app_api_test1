import 'package:flutter/material.dart';

class ComponentCountTitle extends StatelessWidget {
  const ComponentCountTitle({
    super.key,
    required this.icon,
    required this.count,
    required this.unitName,
    required this.itemName
  });

  final IconData icon;
  final int count;  // 몇개
  final String unitName; // 몇건
  final String itemName; // 몇건에 무엇인지 이름

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Icon(
            icon,
            size: 30,
          ),
          const SizedBox(
            width: 10,
          ),
          Text('총 ${count.toString()} $unitName의 $itemName이(가) 있습니다.',
            style: TextStyle(
              fontSize: 20,
              fontFamily: 'maple'
            ),
          ),
        ],
      ),
    );
  }
}
