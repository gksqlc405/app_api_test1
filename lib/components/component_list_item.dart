import 'package:app_api_test1/model/hospital_list.dart';
import 'package:app_api_test1/model/hospital_list_item.dart';
import 'package:flutter/material.dart';

class ComponentListItem extends StatelessWidget {
  const ComponentListItem({
    super.key,
    required this.item,
    required this.callback
  });

  final HospitalListItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.pink),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text('${item.orgnm == null ? '병원이름이 없습니다.' : item.orgnm}',
            style: TextStyle(
              fontFamily: 'maple'
            ),
            ),
            Text('${item.lunchSttTm == null ? '점심시간 정보가 없습니다' : item.lunchSttTm}',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
            ),
            Text('${item.slrYmd == null ? '없습니다.' : item.slrYmd}',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
            ),
            Text('${item.orgTlno == null ? '없습니다.' : item.orgTlno}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text('${item.dywk == null ? '없습니다.' : item.dywk}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text('${item.hldyYn == null ? '없습니다.' : item.hldyYn}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
